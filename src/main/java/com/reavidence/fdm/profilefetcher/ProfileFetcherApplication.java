package com.reavidence.fdm.profilefetcher;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProfileFetcherApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProfileFetcherApplication.class, args);
	}
}
