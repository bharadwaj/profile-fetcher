package com.reavidence.fdm.profilefetcher.seleniumOperations;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sandeep on 4/20/2018.
 */
public class CSVReader {

    public static void main(String[] args) {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader("F://FDMEXTR.csv"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        List<String> lines = new ArrayList<>();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
               // System.out.println(line);
                String[] animalsArray = line.split(",");
                System.out.println(animalsArray[0]);
                System.out.println(animalsArray[1]);
                lines.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        //System.out.println(lines.get(0));
    }
}
