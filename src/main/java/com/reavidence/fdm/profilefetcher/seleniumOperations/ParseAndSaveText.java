package com.reavidence.fdm.profilefetcher.seleniumOperations;

import com.reavidence.fdm.profilefetcher.Repository.*;
import com.reavidence.fdm.profilefetcher.model.*;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Scanner;

/**
 * Created by Sandeep on 3/12/2018.
 */
@Service
public class ParseAndSaveText {

    //Session session = null;
   /* Transaction transaction = null;*/

    @Autowired
    private AcademicAffiliationRepository academicAffiliationRepository;

    @Autowired
    private CertificationRepository certificationRepository;

    @Autowired
    private HonorRepository honorRepository;

    @Autowired
    private ExperienceRepository experienceRepository;

    @Autowired
    private SchoolAttendedRepository schoolAttendedRepository;

    @Autowired
    private SummaryRepository summaryRepository;

    @Autowired
    private SkillRepository skillRepository;

    @Autowired
    private PublicationsRepository publicationsRepository;

    @Autowired
    private KeyContactsRepository keyContactsRepository;

    @Autowired
    private AccomplishmentRepository accomplishmentRepository;


    /*Parse about*/
       /*Parse about*/
    public Boolean extractExperience(String s, org_keycontacts keyContact) {
        // link=link+"https://www.linkedin.com/in/ACoAAADquDwBXw78SFvBrWKrQemdBhrRhHC0EQs/";
        Scanner scanner = new Scanner(s);

        String designation = "";
        String company = "";
        String duration = "";
        String data = "";
        int count = 0;
        String split_lines[] = new String[10000];

        while (scanner.hasNextLine()) {
            split_lines[count] = scanner.nextLine();
            count++;
        }


        int startOfDesc = 0;
        int endOfDesc = 0;

        for (int i = 0; i < split_lines.length - 1; i++) {

            if (split_lines[i] != null && split_lines[i].equals("Company Name")) {

                //org_keycontacts keycontacts = keyContactsRepository.getByLinkedInProfile(link);
                System.out.println(split_lines[i - 1]);
                //designation = split_lines[i - 1];
                org_experience orgexperience = new org_experience();
                orgexperience.setDesignation(split_lines[i - 1]);
                endOfDesc = i - 1;
                System.out.println("Company:============================");
                System.out.println(split_lines[i + 1]);
                //company = split_lines[i + 1];
                orgexperience.setCompany_name(split_lines[i + 1]);
                //orgexperience.setR_status(5);
                i++;
                i++;

                if (split_lines[i] != null && split_lines[i].equals("Dates Employed")) {
                    i++;
                    System.out.println("Dates Emp:============================");
                    System.out.println(split_lines[i]);
                    orgexperience.setDates_employed(split_lines[i]);

                    i++;

                }
                if (split_lines[i] != null && split_lines[i].equals("Employment Duration")) {
                    i++;
                    System.out.println("Duration:============================");
                    System.out.println(split_lines[i]);
                    duration = split_lines[i];
                    orgexperience.setDuration(split_lines[i]);
                    i++;
                }
                if (split_lines[i] != null && split_lines[i].equals("Location")) {
                    i++;
                    System.out.println("Location:============================");
                    System.out.println(split_lines[i]);
                    orgexperience.setLocation(split_lines[i]);
                    System.out.println("desc:============================ ");
                    String description = "";
                    while (split_lines[i + 2] != null && !split_lines[i + 2].equals("Company Name")) {
                        i++;
                        System.out.println(split_lines[i]);
                        data = split_lines[i];
                        description = description + data;

                    }
                    orgexperience.setData(description);

                }
                System.out.println("In Experience: "+i);
                orgexperience.setContact(keyContact);
                experienceRepository.save(orgexperience);
            }


        }

        return true;
    }

    public Boolean extractStudy(String s,org_keycontacts keyContact) {
        // link=link+"https://www.linkedin.com/in/ACoAAADquDwBXw78SFvBrWKrQemdBhrRhHC0EQs/";

        Scanner scanner = new Scanner(s);
        String university = "";
        String degree = "";
        String duration = "";
        String stream = "";
        int count = 0;
        String split_lines[] = new String[10000];

        while (scanner.hasNextLine()) {
            split_lines[count] = scanner.nextLine();
            count++;
        }


        int startOfDesc = 0;
        int endOfDesc = 0;
        org_school_attended schoolattended;
        for (int i = 0; i < split_lines.length - 1; i++) {

            if (split_lines[i] != null && split_lines[i].equals("Degree Name")) {
                //org_keycontacts keycontacts = keyContactsRepository.getByLinkedInProfile(link);
                System.out.println("University:=========================");
                System.out.println(split_lines[i - 1]);
                university = split_lines[i - 1];
                endOfDesc = i - 1;
                schoolattended = new org_school_attended();
                schoolattended.setUniversity(university);
                System.out.println("stream:============================");
                System.out.println(split_lines[i + 1]);
                degree = split_lines[i + 1];
                schoolattended.setDegree(degree);
                schoolattended.setR_status(1);
                i++;
                i++;
                if (split_lines[i] != null && split_lines[i].equals("Field Of Study")) {
                    i++;
                    System.out.println("Type of degree:=========================");
                    System.out.println(split_lines[i]);
                    stream = split_lines[i];

                    schoolattended.setStream(stream);
                    i = i + 4;
                    System.out.println("Duration:====================================");
                    duration = split_lines[i];
                    schoolattended.setDuration(duration);
                }
                schoolattended.setContact(keyContact);

                schoolAttendedRepository.save(schoolattended);


            }


        }
        return true;
    }

    public Boolean extractVolExperience(String s,org_keycontacts keyContact) {

        Scanner scanner = new Scanner(s);
        String designation = "";
        String company = "";
        String duration = "";
        String data = "";
        int count = 0;
        String split_lines[] = new String[10000];

        while (scanner.hasNextLine()) {
            split_lines[count] = scanner.nextLine();
            count++;
        }

        int startOfDesc = 0;
        int endOfDesc = 0;
        org_academic_affiliation academicaffiliation;
        for (int i = 0; i < split_lines.length - 1; i++) {
            if (split_lines[i] != null && split_lines[i].equals("Company Name")) {

                // String sql = "SELECT p FROM Person p WHERE p.person_url = :link";

               // org_keycontacts keycontacts = keyContactsRepository.getByLinkedInProfile(link);
                System.out.println("designation:==========================");
                System.out.println(split_lines[i - 1]);
                designation = split_lines[i - 1];
                endOfDesc = i - 1;
                System.out.println("Company Name:===========================");
                System.out.println(split_lines[i + 1]);
                company = split_lines[i + 1];
                i++;
                i++;
                academicaffiliation = new org_academic_affiliation();
                academicaffiliation.setVolunteer(designation);
                academicaffiliation.setCompany_name(company);
                academicaffiliation.setR_status(1);
                if (split_lines[i] != null && split_lines[i].equals("Dates volunteered")) {
                    i++;
                    System.out.println("Dates volunteered:=========================");
                    System.out.println(split_lines[i]);
                    academicaffiliation.setDates_volunteered(split_lines[i]);
                    i++;

                }

                if (split_lines[i] != null && split_lines[i].equals("Volunteer duration")) {
                    i++;
                    System.out.println("Duration:==========================");
                    System.out.println(split_lines[i]);
                    duration = split_lines[i];
                    academicaffiliation.setDuration(duration);
                    i++;
                }
                if (split_lines[i] != null && split_lines[i].equals("Cause")) {
                    i++;
                    System.out.println("Cause:=========================");
                    System.out.println(split_lines[i]);
                    academicaffiliation.setCause(split_lines[i]);
                    System.out.println("desc:============================ ");
                    i++;
                    System.out.println(split_lines[i]);
                    academicaffiliation.setDescription(split_lines[i]);

                }
                academicaffiliation.setContact(keyContact);

                academicAffiliationRepository.save(academicaffiliation);


            }


        }
        return true;
    }


    public Boolean fetchSkill(List<WebElement> element,org_keycontacts keyContact) {

        org_skill skills;
        for (WebElement skill : element) {

            // String sql = "SELECT p FROM Person p WHERE p.person_url = :link";
            //org_keycontacts keycontacts = keyContactsRepository.getByLinkedInProfile(link);
            skills = new org_skill();
            skills.setSkill(skill.getText());
            skills.setR_status(1);
            skills.setContact(keyContact);


            skillRepository.save(skills);

        }
        return true;
    }

    public Boolean fetchCertification(String s,org_keycontacts keyContact) {
        //link=link+"https://www.linkedin.com/in/ACoAAADquDwBXw78SFvBrWKrQemdBhrRhHC0EQs/";
        Scanner scanner = new Scanner(s);
        int count = 0;
        String split_lines[] = new String[10000];

        while (scanner.hasNextLine()) {
            split_lines[count] = scanner.nextLine();
            count++;
        }
        org_certification org_certification;
        for (int i = 0; i < split_lines.length - 1; i++) {

            if (split_lines[i] != null && split_lines[i].equals("Title")) {


               // org_keycontacts keycontacts = keyContactsRepository.getByLinkedInProfile(link);
                i++;
                System.out.println("Title:============================");
                org_certification = new org_certification();
                System.out.println(split_lines[i]);
                org_certification.setName(split_lines[i]);
                i = i + 2;
                System.out.println(split_lines[i]);
                org_certification.setDate_of_issue(split_lines[i]);
                i = i + 2;
                org_certification.setCertification_authority(split_lines[i]);
                org_certification.setR_status(1);
                System.out.println(split_lines[i]);
                org_certification.setContact(keyContact);
                org_accomplishment accomplishment = accomplishmentRepository.findByTypeOfAccomplishment("Certifications");
                org_certification.setAccomplishment(accomplishment);

                certificationRepository.save(org_certification);

            }
        }
        org_honor_award honorAward;
        for (int i = 0; i < split_lines.length - 1; i++) {

            if (split_lines[i] != null && split_lines[i].equals("honor title")) {

               // org_keycontacts keycontacts = keyContactsRepository.getByLinkedInProfile(link);
                i++;
                System.out.println("Title:============================");
                honorAward = new org_honor_award();
                System.out.println(split_lines[i]);
                honorAward.setTitle(split_lines[i]);
                i = i + 2;
                System.out.println(split_lines[i]);
                honorAward.setDate_of_issue(split_lines[i]);
                i = i + 2;
                honorAward.setIssuer(split_lines[i]);
                i = i + 2;
                honorAward.setDescription(split_lines[i]);
                honorAward.setR_status(1);
                System.out.println(split_lines[i]);
                honorAward.setContact(keyContact);
                org_accomplishment accomplishment = accomplishmentRepository.findByTypeOfAccomplishment("Honors & Awards");
                honorAward.setAccomplishment(accomplishment);

                honorRepository.save(honorAward);


            }

        }
        org_publication publications;
        for (int i = 0; i < split_lines.length - 1; i++) {
            if (split_lines[i] != null && split_lines[i].equals("publication title")) {

             //   org_keycontacts keycontacts = keyContactsRepository.getByLinkedInProfile(link);
                i++;
                System.out.println("Title:============================");
                publications = new org_publication();
                System.out.println(split_lines[i]);
                publications.setTitle(split_lines[i]);
                i = i + 2;
                System.out.println(split_lines[i]);
                publications.setDate_of_issue(split_lines[i]);
                i = i + 2;
                publications.setDescription(split_lines[i]);
                publications.setR_status(1);
                System.out.println(split_lines[i]);
                publications.setOrg_key_contacts(keyContact);
                org_accomplishment accomplishment = accomplishmentRepository.findByTypeOfAccomplishment("Publications");
                publications.setAccomplishment(accomplishment);

                publicationsRepository.save(publications);


            }
        }
        return true;
    }

    public void saveSummary(String name, String head_line, String location,org_keycontacts keyContact) {

        //System.out.println(link);
       // org_keycontacts keycontacts = keyContactsRepository.getByLinkedInProfile(link);
        // System.out.println(keycontacts.getLinkedInProfile());

        org_summary org_summary = new org_summary();
        org_summary.setName(name);
        org_summary.setHead_line(head_line);
        org_summary.setLocation(location);
        org_summary.setR_status(1);
        org_summary.setOrg_key_contacts(keyContact);

        summaryRepository.save(org_summary);


    }


}


  /*  public Boolean extractSkill(String s, String link) {
        Scanner scanner = new Scanner(s);
        int count = 0;
        String split_lines[] = new String[10000];

        while (scanner.hasNextLine()) {
            split_lines[count] = scanner.nextLine();
            count++;
        }
        org_skill skills ;
        for (int i = 0; i < split_lines.length - 1; i++) {
            if (split_lines[i] != null&& !split_lines[i].equals("From Skills.")) {
                Session session= connectStatement();
                // String sql = "SELECT p FROM Person p WHERE p.person_url = :link";
                Query query=session.createQuery("SELECT p FROM Person  p WHERE p.person_url = :link");
                query.setParameter("link", link);
                System.out.println("Skills:");
                System.out.println(split_lines[i]);
                skills=new org_skill();
                skills.setSkill(split_lines[i]);

                List<Person> persons= query.list();
                for (Person p:persons) {
                    skills.setPerson(p);
                    System.out.println(p+"===========================");
                }
                session.beginTransaction();

                session.save(skills);

                session.getTransaction().commit();
                session.close();
            }

        }

        return true;
    } */


