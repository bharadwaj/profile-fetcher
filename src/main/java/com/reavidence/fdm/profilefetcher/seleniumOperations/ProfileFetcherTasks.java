package com.reavidence.fdm.profilefetcher.seleniumOperations;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import com.reavidence.fdm.profilefetcher.Repository.CompletedProfilesRepository;
import com.reavidence.fdm.profilefetcher.Repository.KeyContactsRepository;
import com.reavidence.fdm.profilefetcher.model.org_completed_profiles;
import com.reavidence.fdm.profilefetcher.model.org_keycontacts;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ProfileFetcherTasks {

    /*public ProfileFetcherTasks(int type){
        System.out.println("Hello World after 30 secs");
        try {
            Thread.sleep(20000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
*/
    @Autowired
    private Operations utils;

    @Autowired
    private KeyContactsRepository keyContactsRepository;

    @Autowired
    private CompletedProfilesRepository completedProfilesRepository;
    public void ProfileFetcher(ArrayList<org_keycontacts> arrayList,Integer login_account) {
        Logger LOG = (Logger) org.slf4j.LoggerFactory.getLogger(ch.qos.logback.classic.Logger.ROOT_LOGGER_NAME);
        LOG.setLevel(Level.WARN);

        ArrayList<String> urlList = new ArrayList<String>();

        
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        WebDriver driver = new ChromeDriver();


        utils.performLogin(driver, login_account);

        for (org_keycontacts keyContact : arrayList) {
        	String url=keyContact.getLinkedInProfile();
            driver.navigate().to(url);
            utils.sleepNSeconds(3);

            //Scroll till ending to load all the data.
            ((JavascriptExecutor) driver)
                    .executeScript("window.scrollTo(0, document.body.scrollHeight)");

            utils.sleepNSeconds(3);

            //TODO uncomment
          /*  if (driver.findElements(By.cssSelector("div.presence-entity__image")).size() > 0) {
                WebElement imageButton = driver.findElement(By.cssSelector("div.presence-entity__image"));
               utils.extractImage(driver,url,imageButton,keyContact);

            }*/


            //Get Description Profile Summary.

            if (driver.findElements(By.className("pv-top-card-section__summary")).size() > 0) {
                System.out.println("Summary:==========================");
                WebElement summary = driver.findElement(By.className("pv-top-card-section__summary"));
                utils.extractSummaryFields(summary,driver,url,keyContact);
                }

            //Check if org_experience is present.
            //TODO initialize experience section ID.
            System.out.println("+++++++++++++++++++++++++++++++++"+driver.findElements(By.id("experience-section")).size()+"+++++++++++++++++++++");
            if (driver.findElements(By.id("experience-section")).size() > 0) {
                System.out.println("org_experience:==========================");
                WebElement experience = driver.findElement(By.id("experience-section"));
                utils.extractExperience(experience, driver,url,keyContact);
            }

            if (driver.findElements(By.className("volunteering-section")).size() > 0) {
                System.out.println("Volunteer org_experience:==========================");
                WebElement experience = driver.findElement(By.className("volunteering-section"));
                utils.extractVolExperience(experience, driver,url,keyContact);
            }

            //Check if Education is present.
            if (driver.findElements(By.id("education-section")).size() > 0) {
                System.out.println("Education:==========================");
                WebElement educationSection = driver.findElement(By.id("education-section"));
                utils.extractEducation(educationSection, driver,url,keyContact);
            }

            //Check if Skills and Endorsements are present.
            //TODO initialize a skills classname string.
            if (driver.findElements(By.className("pv-featured-skills-section")).size() > 0) {
                System.out.println("Skills:==========================");
                WebElement skills = driver.findElement(By.className("pv-featured-skills-section"));
                utils.extractSkills(skills, driver, 1,url,keyContact);

            }else if (driver.findElements(By.className("pv-org_skill-categories-section")).size() > 0){
                System.out.println("Skills:==========================");
                WebElement skillsSection = driver.findElement(By.className("pv-org_skill-categories-section"));
                utils.extractSkills(skillsSection, driver, 2,url,keyContact);
            }else if(driver.findElements(By.className("pv-skill-categories-section")).size() > 0){
                WebElement skillsSection = driver.findElement(By.className("pv-skill-categories-section"));
                utils.extractSkills(skillsSection, driver, 2,url,keyContact);
            }



            utils.sleepNSeconds(3);

            //Check if Accomplishment is present.
            if (driver.findElements(By.className("pv-accomplishments-section")).size() > 0) {
                List<WebElement> a=driver.findElements(By.cssSelector("button.pv-accomplishments-block__expand")) ;

                if (driver.findElements(By.cssSelector(".pv-accomplishments-block.publications")).size() > 0) {
                    System.out.println("publications:==========================");
                    WebElement accomplishmentSction = driver.findElement(By.cssSelector(".pv-accomplishments-block.publications"));
                    utils.extractAccomplishment(accomplishmentSction, driver,url,keyContact);
                }

                if (driver.findElements(By.cssSelector(".pv-accomplishments-block.projects")).size() > 0) {
                    System.out.println("projects:==========================");
                    WebElement accomplishmentSction = driver.findElement(By.cssSelector(".pv-accomplishments-block.projects"));
                    utils.extractAccomplishment(accomplishmentSction, driver,url,keyContact);
                }
                if (driver.findElements(By.cssSelector(".pv-accomplishments-block.certifications")).size() > 0) {
                    System.out.println("certifications:==========================");
                    WebElement accomplishmentSction = driver.findElement(By.cssSelector(".pv-accomplishments-block.certifications"));
                    utils.extractAccomplishment(accomplishmentSction, driver,url,keyContact);
                }
                if (driver.findElements(By.cssSelector(".pv-accomplishments-block.honors")).size() > 0) {
                    System.out.println("Honors&award:==========================");
                    WebElement accomplishmentSction = driver.findElement(By.cssSelector(".pv-accomplishments-block.honors"));
                    utils.extractAccomplishment(accomplishmentSction, driver,url,keyContact);

                }
            }


            String name = null;
            if(login_account == 1){
               name= "Mario";
            }else if(login_account == 2){
                name= "sandeep";
            }else if(login_account == 3){
                name= "pavan";
            }else if(login_account == 4){
                name="jaya";
            }else if(login_account == 5){
                name="shankar";
            }
            else if(login_account ==  7){
                name="rama";
            }
            else {
                // TODO exit. Not a valid account.
            }
            org_completed_profiles orgCompletedProfiles= completedProfilesRepository.findByName(name);
            System.out.println("name______________________"+orgCompletedProfiles.getName());
           // org_keycontacts keycontacts = keyContactsRepository.getByLinkedInProfile(url);
           // System.out.println("key id______________"+keycontacts);
            orgCompletedProfiles.setContact(keyContact);
            completedProfilesRepository.save(orgCompletedProfiles);
        }


    }


}
