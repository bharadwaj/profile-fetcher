package com.reavidence.fdm.profilefetcher.model;

/**
 * Created by Sandeep on 3/16/2018.
 */
import javax.persistence.*;

/**
 * Created by Sandeep on 1/3/2018.
 */
@Entity
public class org_academic_affiliation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String volunteer;
    private String company_name;
    private String duration;
    private String dates_volunteered;
    private String cause;
    @Column(columnDefinition="LONGTEXT")
    private String description;

    @ManyToOne
    private org_keycontacts contact;

    private Integer r_status;

    public Integer getR_status() {
        return r_status;
    }

    public void setR_status(Integer r_status) {
        this.r_status = r_status;
    }

    public org_keycontacts getContact() {
        return contact;
    }

    public void setContact(org_keycontacts orgkeycontacts) {
        this.contact = orgkeycontacts;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVolunteer() {
        return volunteer;
    }

    public void setVolunteer(String volunteer) {
        this.volunteer = volunteer;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }


    public String getDates_volunteered() {
        return dates_volunteered;
    }

    public void setDates_volunteered(String dates_volunteered) {
        this.dates_volunteered = dates_volunteered;
    }

    public String getCause() {
        return cause;
    }

    public void setCause(String cause) {
        this.cause = cause;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


}
