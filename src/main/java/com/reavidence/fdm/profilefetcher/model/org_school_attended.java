package com.reavidence.fdm.profilefetcher.model;


import javax.persistence.*;

/**
 * Created by Sandeep on 12/21/2017.
 */
@Entity
@Table
public class org_school_attended {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String university;
    private String stream;
    private String duration;

    @Column(columnDefinition="LONGTEXT")
    private String degree;

    @ManyToOne
    private org_keycontacts contact;

    private Integer r_status;

    public Integer getR_status() {
        return r_status;
    }

    public void setR_status(Integer r_status) {
        this.r_status = r_status;
    }


    public org_keycontacts getContact() {
        return contact;
    }

    public void setContact(org_keycontacts orgkeycontacts) {
        this.contact = orgkeycontacts;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    public String getStream() {
        return stream;
    }

    public void setStream(String stream) {
        this.stream = stream;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }


    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }


}

