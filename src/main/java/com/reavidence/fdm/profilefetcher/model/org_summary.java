package com.reavidence.fdm.profilefetcher.model;

import javax.persistence.*;

/**
 * Created by Sandeep on 4/3/2018.
 */
@Entity
public class org_summary {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String head_line;
    private String location;
    @ManyToOne
    private org_keycontacts org_key_contacts;

    private Integer r_status;

    public Integer getR_status() {
        return r_status;
    }

    public void setR_status(Integer r_status) {
        this.r_status = r_status;
    }


    public org_keycontacts getOrg_key_contacts() {
        return org_key_contacts;
    }

    public void setOrg_key_contacts(org_keycontacts orgkeycontacts) {
        this.org_key_contacts = orgkeycontacts;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHead_line() {
        return head_line;
    }

    public void setHead_line(String head_line) {
        this.head_line = head_line;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

}
