package com.reavidence.fdm.profilefetcher.model;

import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;

import javax.persistence.*;

/**
 * Created by Sandeep on 3/20/2018.
 */
@Entity
@Indexed
public class org_skill {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Field
    private String skill;

    @ManyToOne
    private org_keycontacts contact;

    private Integer r_status;

    public Integer getR_status() {
        return r_status;
    }

    public void setR_status(Integer r_status) {
        this.r_status = r_status;
    }


    public org_keycontacts getContact() {
        return contact;
    }

    public void setContact(org_keycontacts orgkeycontacts) {
        this.contact = orgkeycontacts;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSkill() {
        return skill;
    }

    public void setSkill(String skill) {
        this.skill = skill;
    }

}
