package com.reavidence.fdm.profilefetcher.model;

import javax.persistence.*;

/**
 * Created by Sandeep on 4/2/2018.
 */
@Entity
public class org_honor_award {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(columnDefinition="LONGTEXT")
    private String title;
    private String date_of_issue;
    private String issuer;
    @Column(columnDefinition="LONGTEXT")
    private String description;
    @ManyToOne
    private org_accomplishment accomplishment;
    @ManyToOne
    private org_keycontacts contact;

    private Integer r_status;

    public Integer getR_status() {
        return r_status;
    }

    public void setR_status(Integer r_status) {
        this.r_status = r_status;
    }


    public org_keycontacts getContact() {
        return contact;
    }

    public void setContact(org_keycontacts orgkeycontacts) {
        this.contact = orgkeycontacts;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate_of_issue() {
        return date_of_issue;
    }

    public void setDate_of_issue(String date_of_issue) {
        this.date_of_issue = date_of_issue;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public org_accomplishment getAccomplishment() {
        return accomplishment;
    }

    public void setAccomplishment(org_accomplishment accomplishment) {
        this.accomplishment = accomplishment;
    }

    public String getIssuer() {
        return issuer;
    }

    public void setIssuer(String issuer) {
        this.issuer = issuer;
    }
}
