package com.reavidence.fdm.profilefetcher.model;

import javax.persistence.*;

/**
 * Created by Sandeep on 4/2/2018.
 */
@Entity
public class org_certification {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(columnDefinition="LONGTEXT")
    private String name;
    private String date_of_issue;
    @Column(columnDefinition="LONGTEXT")
    private String certification_authority;
    @ManyToOne
    private org_accomplishment accomplishment;
    @ManyToOne
    private org_keycontacts contact;

    private Integer r_status;

    public Integer getR_status() {
        return r_status;
    }

    public void setR_status(Integer r_status) {
        this.r_status = r_status;
    }


    public org_keycontacts getContact() {
        return contact;
    }

    public void setContact(org_keycontacts orgkeycontacts) {
        this.contact = orgkeycontacts;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate_of_issue() {
        return date_of_issue;
    }

    public void setDate_of_issue(String date_of_issue) {
        this.date_of_issue = date_of_issue;
    }

    public String getCertification_authority() {
        return certification_authority;
    }

    public void setCertification_authority(String certification_authority) {
        this.certification_authority = certification_authority;
    }

    public org_accomplishment getAccomplishment() {
        return accomplishment;
    }

    public void setAccomplishment(org_accomplishment accomplishment) {
        this.accomplishment = accomplishment;
    }


}
