package com.reavidence.fdm.profilefetcher.model;

import javax.persistence.*;

/**
 * Created by Sandeep on 4/6/2018.
 */
@Entity
public class org_completed_profiles {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;

    @ManyToOne
    private org_keycontacts contact;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public org_keycontacts getContact() {
        return contact;
    }

    public void setContact(org_keycontacts contact) {
        this.contact = contact;
    }
}
