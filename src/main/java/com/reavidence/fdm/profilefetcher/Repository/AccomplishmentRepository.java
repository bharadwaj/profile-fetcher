package com.reavidence.fdm.profilefetcher.Repository;

import com.reavidence.fdm.profilefetcher.model.org_accomplishment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Created by Sandeep on 4/5/2018.
 */
public interface AccomplishmentRepository extends JpaRepository<org_accomplishment,Long> {

    @Query("select p from org_accomplishment p where p.type_of_accomplishment=:name")
    public org_accomplishment findByTypeOfAccomplishment(@Param("name") String name);
}
