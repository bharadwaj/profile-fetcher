package com.reavidence.fdm.profilefetcher.Repository;

import com.reavidence.fdm.profilefetcher.model.org_keycontacts;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sandeep on 4/5/2018.
 */
@Repository
public interface KeyContactsRepository extends JpaRepository<org_keycontacts,Long> {

    @Query("select o from  org_keycontacts o where o.LinkedInProfile=:LinkedInProfile")
    public org_keycontacts getByLinkedInProfile(@Param("LinkedInProfile") String LinkedInProfile);

    @Query("select p from org_keycontacts p where p.id>=:startId and p.id<=:endId AND p.LinkedInProfile != ''")
    public ArrayList<org_keycontacts> getListOfContacts(@Param("startId") Integer stardId, @Param("endId") Integer endId);

    @Query("select p.id from org_keycontacts p where p.id>:startId AND p.id<=:endId AND p.LinkedInProfile != '' ORDER BY p.id ASC ")
    public List<Integer> getMaxStartingLink(@Param("startId") Integer stardId, @Param("endId") Integer endId);

    @Query("select o from  org_keycontacts o where o.LinkedInProfile=:LinkedInProfile")
    public List<org_keycontacts> getListByLinkedInProfile(@Param("LinkedInProfile") String LinkedInProfile);


}
