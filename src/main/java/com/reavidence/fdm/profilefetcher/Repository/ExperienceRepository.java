package com.reavidence.fdm.profilefetcher.Repository;

import com.reavidence.fdm.profilefetcher.model.org_experience;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Sandeep on 4/5/2018.
 */
public interface ExperienceRepository extends JpaRepository<org_experience,Long> {
}
