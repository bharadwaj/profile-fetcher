package com.reavidence.fdm.profilefetcher.Repository;

import com.reavidence.fdm.profilefetcher.model.org_academic_affiliation;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Sandeep on 4/5/2018.
 */
public interface AcademicAffiliationRepository extends JpaRepository<org_academic_affiliation,Long> {
}
