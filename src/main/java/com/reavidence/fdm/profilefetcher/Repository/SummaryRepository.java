package com.reavidence.fdm.profilefetcher.Repository;

import com.reavidence.fdm.profilefetcher.model.org_summary;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Sandeep on 4/5/2018.
 */
public interface SummaryRepository extends JpaRepository<org_summary,Long> {
}
