package com.reavidence.fdm.profilefetcher.Repository;

import com.reavidence.fdm.profilefetcher.model.org_honor_award;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Sandeep on 4/5/2018.
 */
public interface HonorRepository extends JpaRepository<org_honor_award,Long> {
}
