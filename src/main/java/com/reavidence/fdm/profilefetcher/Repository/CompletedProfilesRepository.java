package com.reavidence.fdm.profilefetcher.Repository;

import com.reavidence.fdm.profilefetcher.model.org_completed_profiles;
import com.reavidence.fdm.profilefetcher.model.org_keycontacts;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

/**
 * Created by Sandeep on 4/6/2018.
 */
public interface CompletedProfilesRepository extends JpaRepository<org_completed_profiles,Integer> {

    @Query("select o.contact from  org_completed_profiles o where o.name = :name")
    public org_keycontacts getMaxContactId(@Param("name") String name);

    @Query("select  o from org_completed_profiles o  where o.name=:name")
    public org_completed_profiles findByName(@Param("name") String name);
}
