package com.reavidence.fdm.profilefetcher.Repository;

import com.reavidence.fdm.profilefetcher.model.org_school_attended;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Sandeep on 4/5/2018.
 */
public interface SchoolAttendedRepository extends JpaRepository<org_school_attended,Long> {
}
