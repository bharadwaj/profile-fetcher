package com.reavidence.fdm.profilefetcher.Controller;

import com.reavidence.fdm.profilefetcher.Repository.CompletedProfilesRepository;
import com.reavidence.fdm.profilefetcher.Repository.KeyContactsRepository;
import com.reavidence.fdm.profilefetcher.model.org_keycontacts;
import com.reavidence.fdm.profilefetcher.model.org_skill;
import com.reavidence.fdm.profilefetcher.seleniumOperations.ProfileFetcherTasks;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sandeep on 4/5/2018.
 */
@RestController
public class KeyContactsController {

    @Autowired
    private KeyContactsRepository keyContactsRepository;

    @Autowired
    ProfileFetcherTasks profileFetcherTasks;

    @Autowired
    CompletedProfilesRepository completedProfilesRepository;

    @Autowired
    EntityManagerFactory entityManagerFactory;

    @GetMapping(value = {"/insert/{account}/{endId}"})
    public ResponseEntity createdata(@PathVariable int account, @PathVariable Integer endId) throws InterruptedException {


        String name = "";
        if (account == 1) {
            name = "Mario";
        } else if (account == 2) {
            name = "sandeep";
        } else if (account == 3) {
            name = "pavan";
        } else if (account == 4) {
            name = "jaya";
        } else if (account == 5) {
            name = "shankar";
        }else if(account ==  7){
            name = "rama";
        }

        org_keycontacts max_contactId = completedProfilesRepository.getMaxContactId(name);

        System.out.println(max_contactId.getId() + " jaaaaaaaaaaaaaaaaaaaaaaaaaaa");
        Integer id = max_contactId.getId();
        //TODO add repository
        List<Integer> toPickTheMaxId = keyContactsRepository.getMaxStartingLink(id, endId);
        id = toPickTheMaxId.get(0);

        System.out.println(id + " saaaaaaaaaaaaaaaaaaaaaaaaaaa");
        profileFetcherTasks.ProfileFetcher(keyContactsRepository.getListOfContacts(id, endId), account);


        return new ResponseEntity("check exp table", HttpStatus.OK);

    }

    @GetMapping(value = "/search/{query}")
    public ResponseEntity searchPosts(@PathVariable String query) {
        //return new ResponseEntity<>(initSearchIndex(), HttpStatus.OK);
        return new ResponseEntity<>(postSearch(query), HttpStatus.OK);
    }

    @GetMapping(value = "/push")
    public ResponseEntity insertCSV() {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader("F://FDMEXTR.csv"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        List<String> lines = new ArrayList<>();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                // System.out.println(line);
                String[] animalsArray = line.split(",");
                System.out.println(animalsArray[0]);
             List keys=   keyContactsRepository.getListByLinkedInProfile(animalsArray[1]);
                System.out.println(keys.size()+"_______________");
                System.out.println(animalsArray[1]);
                org_keycontacts  keycontacts=new org_keycontacts();
                keycontacts.setOrganizationID(animalsArray[0]);
                keycontacts.setLinkedInProfile(animalsArray[1]);
                if(keys.size()>0){
                    System.out.println("already there");
                }else{
                    keyContactsRepository.save(keycontacts);
                }
                lines.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>("saved", HttpStatus.OK);
    }

    private List<org_skill> postSearch(String q) {


        EntityManager em = entityManagerFactory.createEntityManager();
        FullTextEntityManager fullTextEntityManager =
                org.hibernate.search.jpa.Search.getFullTextEntityManager(em);
        em.getTransaction().begin();

// create native Lucene query using the query DSL
// alternatively you can write the Lucene query using the Lucene query parser
// or the Lucene programmatic API. The Hibernate Search DSL is recommended though
        QueryBuilder qb = fullTextEntityManager.getSearchFactory()
                .buildQueryBuilder().forEntity(org_skill.class).get();
        org.apache.lucene.search.Query query = qb
                .keyword()
                .onFields("skill")
                .matching(q)
                .createQuery();

// wrap Lucene query in a javax.persistence.Query
        javax.persistence.Query persistenceQuery =
                fullTextEntityManager.createFullTextQuery(query, org_skill.class);

// execute search
        List<org_skill> result = persistenceQuery.getResultList();

        em.getTransaction().commit();
        em.close();

        return result;
    }

    int initSearchIndex(){
        EntityManager em = entityManagerFactory.createEntityManager();
        FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(em);
        try {
            fullTextEntityManager.createIndexer().startAndWait();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return 1;
    }

}
